#!/bin/bash
ac=`ps aux | grep -i socat | grep -wv grep | awk '{ print $2 }'`
if [ -z $ac ]; then
  sc=`which socat`
  if [ -z $sc ]; then
    echo "This application needs socat installed"
    exit 100
  fi
  socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\" &
fi

xq=`ps aux | grep -i xquartz | grep -wv grep | awk '{ print $2 }'`
if [ -z $xq ]; then
  open -a XQuartz
fi

docker run -it --privileged --net=host -v="$HOME/.Xauthority:/root/.Xauthority:rw" -v="$(pwd)/user:/home/user" -e DISPLAY=docker.for.mac.host.internal:0 codium-container /bin/bash /home/user/launch-codium-X11.sh
