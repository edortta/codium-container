# Codium-container

The purpouse of this project is to deliver a copy of vscodium that can be used locally or remotely trough X11.

This means that you can install it on a powerfull server and use as it is installed locally meaning that you can install nothing in your local computer, or use it from a public computer.

## You don't need it

Remember that if you intend to use [vscodium](https://vscodium.com/) in your local machine and your operative system support it, you don't need this project. Just install vscodium locally and you're ready and - most important - free.

But, if you have a bunch of programmers and in a variety of harwdare/software can be a good option to have the same platform for all of them.

## Prerequisites

If your local computer is running Linux, and you is already using an GUI, then you're in the correct path.

But, if you is still using Windows or MacOS, you will need to install X11 server and `socat` in your machine

In Mac, [XQuartz](https://www.xquartz.org/) has been the right choice for me.

### Remotely

Let's say your server is in 192.168.2.10

Then, you can do

```bash
$ ssh esteban@192.168.2.10 -X
# cd ~/dev
# git clone https://gitlab.com/edortta/codium-container.git
# cd codium-container
# docker build -t codium-container .
# ./launch-remotely.sh
```

These commands copied the project, built it and then lunch the interface as if you were in the server

Aftar that, and in the next times, you just need to do

```bash
$ ssh esteban@192.168.2.10 -X
# cd ~/dev/codium-container
# ./launch-remotely.sh
```

Remember that all of your projects, plugins, etc will be in the server and not in your local machine. That is the purpose.

### Locally

Clone the project in your machine and use. It looks like this:

```bash
$ cd ~/dev
$ git clone https://gitlab.com/edortta/codium-container.git
$ cd codium-container
$ docker build -t codium-container .
$ ./launch-locally.sh
```

After that, you can use it just doing:

```bash
$ cd ~/dev
$ cd codium-container
$ ./launch-locally.sh
```

All your projects will be in local machine. There is a shared folder called  `user` that can hold these.



#### Prerequisites on Mac

You can easly install socat in Mac using [MacPorts](https://ports.macports.org/) like this:

```bash
sudo port install socat
```
And XQuartz can be downloaded and installed or directly using `port`

```bash
sudo port install quartz-wm
```

Start XQuartz

```bash
open -a XQuartz
```

##### Manually usage at your local computer

Launch `socat`

```bash
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
```

***Keep that terminal open*** 

In another terminal, start this `codium`:

```bash
docker run \
  -it \
  --privileged \
  --net=host \
  -v="$HOME/.Xauthority:/root/.Xauthority:rw" \
  -v="$(pwd)/user:/home/user" \
  -e DISPLAY=docker.for.mac.host.internal:0 \
  codium-container
```


