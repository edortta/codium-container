FROM ubuntu:22.04
LABEL maintainer="edortta71@gmail.com"

ENV DEBIAN_FRONTEND=noninteractive

RUN dpkg --add-architecture i386
RUN apt-get update -y

RUN apt-get install -y curl zip unzip wget
RUN apt-get install -y xorg xvfb gtk2-engines-pixbuf
RUN apt-get -y install dbus-x11 xfonts-base xfonts-100dpi xfonts-75dpi xfonts-cyrillic xfonts-scalable

RUN apt-get install -y libz1 libncurses5 libbz2-1.0:i386 libstdc++6 libbz2-1.0 lib32stdc++6 lib32z1
RUN apt-get install -y gnupg

# RUN wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
RUN wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | apt-key add -

# RUN echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' | tee /etc/apt/sources.list.d/vscodium.list
RUN echo 'deb https://download.vscodium.com/debs vscodium main' | tee --append /etc/apt/sources.list.d/vscodium.list

RUN apt-get update -y

RUN apt-get install -y codium

# Clean up
RUN apt-get clean
RUN apt-get purge

RUN echo 'kernel.unprivileged_userns_clone=1' > /etc/sysctl.d/00-local-userns.conf
RUN service procps restart

RUN useradd -ms /bin/bash user

ENV DISPLAY :0
USER user

RUN mkdir -p /home/user/.codium
RUN echo "keycode 22 = e" > /home/user/.Xmodmap

# ENTRYPOINT ["/bin/sh", "-c", "$0 \"$@\"", "/usr/bin/codium", "--wait"]

