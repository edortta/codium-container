#!/bin/bash
docker run -it --privileged  --net=host --env="DISPLAY" -v /tmp/.X11-unix:/tmp/.X11-unix -v"$(pwd)/user:/home/user" -v"$(pwd)/project:/home/user/project" -v "$HOME/.Xauthority:/home/user/.Xauthority:rw" codium-container /bin/bash /home/user/launch-codium-X11.sh
